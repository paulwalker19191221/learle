<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
use Illuminate\Support\Facades\Redirect;
session_start();

class AdminController extends Controller
{
   public function index(){
   		return view ('admin_login');
   }

    public function show_dashboard(){
   		return view ('admin.dashboard');
   }
   public function dashboard(Request $Request){
   		$admin_email = $Request->admin_mail;
   		$admin_password = md5($Request->admin_password);
   		//dump($admin_email);
   		$result = DB::table('tbl_admin')->where('admin_email',$admin_email)->where('admin_password',$admin_password)->first();
   		/*echo '<prev>';
   		print_r($result);
   		echo '</prev>';*/
   		if($result){
   			Session::put('admin_name',$result->admin_name);
   			Session::put('admin_id',$result->id);
   			return Redirect::to('/dashboard');
   		}
   		else{
   			Session::put('message','Mật khẩu or email sai cmnr. Nhập lại thử xem');
   			//dump(message);
   			return Redirect::to('/Admin');
   		}
   	}
   	public function logout(){
   		    Session::put('admin_name',null);
   			Session::put('admin_id',null);
   			return Redirect::to('/Admin');
   	}
}
